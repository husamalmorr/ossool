<?php 
// Include configuration file   
require_once 'config.php'; 
 
// Include Stripe PHP library  
require_once 'stripe-php/init.php'; 
 
// Set API key 
\Stripe\Stripe::setApiKey('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'); 
 
$response = array( 
    'status' => 0, 
    'error' => array( 
        'message' => 'Invalid Request!'    
    ) 
); 
 
if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
    $input = file_get_contents('php://input'); 
    $request = json_decode($input);     
} 
 
if (json_last_error() !== JSON_ERROR_NONE) { 
    http_response_code(400); 
    echo json_encode($response); 
    exit; 
} 
 
if(!empty($request->checkoutSession)){ 
    // Create new Checkout Session for the order 
    try { 
        $session = \Stripe\Checkout\Session::create([ 
		
            'payment_method_types' => ['card'], 
            'line_items' => [[ 
                'price_data' => [ 
                    'product_data' => [ 
                        'name' => $productName4, 
                        'metadata' => [ 
                            'pro_id' => $productID4 
                        ] 
                    ], 
                    'unit_amount' => $stripeAmount2, 
                    'currency' => $currency4, 
                ], 
                'quantity' => 1, 
                'description' => $productName4, 
            ]], 
            'mode' => 'payment', 
            'success_url' => STRIPE_SUCCESS_URL.'?session_id={CHECKOUT_SESSION_ID}', 
            'cancel_url' => STRIPE_CANCEL_URL, 
        ]); 
    }catch(Exception $e) {  
        $api_error = $e->getMessage();  
    } 
     
    if(empty($api_error) && $session){ 
        $response = array( 
            'status' => 1, 
            'message' => 'Checkout Session created successfully!', 
            'sessionId' => $session['id'] 
        ); 
    }else{ 
        $response = array( 
            'status' => 0, 
            'error' => array( 
                'message' => 'Checkout Session creation failed! '.$api_error    
            ) 
        ); 
    } 
	
} 
 
// Return response 
echo json_encode($response);