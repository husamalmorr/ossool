<?php

require 'vendor/autoload.php';
\Stripe\Stripe::setApiKey('sk_test_51Irh2xAilp4bm2qgeMJqUGPiz0On4VddhjXN82dpVO02GSh7cRqk6kyfAmCH6ljCE2X5Yrb0YbPQQynS3AkmuEWF00AycfJIMn');

header('Content-Type: application/json');

$YOUR_DOMAIN = 'http://localhost/porsper2';

$checkout_session = \Stripe\Checkout\Session::create([
  'payment_method_types' => ['card'],
  'line_items' => [[
    'price_data' => [
      'currency' => 'usd',
      'unit_amount' => 2000,
      'product_data' => [
        'name' => 'Stubborn Attachments',
        'images' => ["https://i.imgur.com/EHyR2nP.png"],
      ],
    ],
    'quantity' => 1,
  ]],
  'mode' => 'payment',
  'success_url' => $YOUR_DOMAIN . '/success.php',
  'cancel_url' => $YOUR_DOMAIN . '/cancel.php',
]);

echo json_encode(['id' => $checkout_session->id]);