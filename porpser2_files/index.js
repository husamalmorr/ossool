



var buyBtn = document.getElementById('payButton');
var responseContainer = document.getElementById('paymentResponse');
    
// Create a Checkout Session with the selected product
var createCheckoutSession = function (stripe) {
    return fetch("stripe_charge.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            checkoutSession: 1,
        }),
    }).then(function (result) {
        return result.json();
    });
};

// Handle any errors returned from Checkout
var handleResult = function (result) {
    if (result.error) {
        responseContainer.innerHTML = '<p>'+result.error.message+'</p>';
    }
    buyBtn.disabled = false;
    buyBtn.textContent = 'Buy Now';
};

// Specify Stripe publishable key to initialize Stripe.js
var stripe = Stripe('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

buyBtn.addEventListener("click", function (evt) {
    buyBtn.disabled = true;
    buyBtn.textContent = 'Please wait...';
    
    createCheckoutSession().then(function (data) {
        if(data.sessionId){
            stripe.redirectToCheckout({
                sessionId: data.sessionId,
            }).then(handleResult);
        }else{
            handleResult(data);
        }
    });
});
var buyBtn2 = document.getElementById('payButton2');
var responseContainer2 = document.getElementById('paymentResponse');
    
// Create a Checkout Session with the selected product
var createCheckoutSession2 = function (stripe) {
    return fetch("stripe_charge2.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            checkoutSession: 1,
        }),
    }).then(function (result) {
        return result.json();
    });
};

// Handle any errors returned from Checkout
var handleResult2 = function (result) {
    if (result.error) {
        responseContainer2.innerHTML = '<p>'+result.error.message+'</p>';
    }
    buyBtn2.disabled = false;
    buyBtn2.textContent = 'Buy Now';
};

// Specify Stripe publishable key to initialize Stripe.js
var stripe2 = Stripe('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

buyBtn2.addEventListener("click", function (evt) {
    buyBtn2.disabled = true;
    buyBtn2.textContent = 'Please wait...';
    
    createCheckoutSession2().then(function (data) {
        if(data.sessionId){
            stripe2.redirectToCheckout({
                sessionId: data.sessionId,
            }).then(handleResult);
        }else{
            handleResult(data);
        }
    });
});
//payment2



































var buyBtn3 = document.getElementById('payButton3');
var responseContainer3 = document.getElementById('paymentResponse');
    
// Create a Checkout Session with the selected product
var createCheckoutSession3 = function (stripe) {
    return fetch("stripe_charge3.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            checkoutSession: 1,
        }),
    }).then(function (result) {
        return result.json();
    });
};

// Handle any errors returned from Checkout
var handleResult3 = function (result) {
    if (result.error) {
        responseContainer3.innerHTML = '<p>'+result.error.message+'</p>';
    }
    buyBtn3.disabled = false;
    buyBtn3.textContent = 'Buy Now';
};

// Specify Stripe publishable key to initialize Stripe.js
var stripe3 = Stripe('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

buyBtn3.addEventListener("click", function (evt) {
    buyBtn3.disabled = true;
    buyBtn3.textContent = 'Please wait...';
    
    createCheckoutSession3().then(function (data) {
        if(data.sessionId){
            stripe3.redirectToCheckout({
                sessionId: data.sessionId,
            }).then(handleResult);
        }else{
            handleResult(data);
        }
    });
});
var buyBtn4 = document.getElementById('payButton4');
var responseContainer4 = document.getElementById('paymentResponse');
    
// Create a Checkout Session with the selected product
var createCheckoutSession4 = function (stripe) {
    return fetch("stripe_charge4.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            checkoutSession: 1,
        }),
    }).then(function (result) {
        return result.json();
    });
};

// Handle any errors returned from Checkout
var handleResult4 = function (result) {
    if (result.error) {
        responseContainer4.innerHTML = '<p>'+result.error.message+'</p>';
    }
    buyBtn4.disabled = false;
    buyBtn4.textContent = 'Buy Now';
};

// Specify Stripe publishable key to initialize Stripe.js
var stripe4 = Stripe('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

buyBtn4.addEventListener("click", function (evt) {
    buyBtn4.disabled = true;
    buyBtn4.textContent = 'Please wait...';
    
    createCheckoutSession4().then(function (data) {
        if(data.sessionId){
            stripe4.redirectToCheckout({
                sessionId: data.sessionId,
            }).then(handleResult);
        }else{
            handleResult(data);
        }
    });
});
//payment4